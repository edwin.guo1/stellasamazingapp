import * as firebase from 'firebase/app'
import 'firebase/auth'
//import 'firebase/firestore'
import 'firebase/database'

//var firebase = require("firebase")

var config = {
  apiKey: "AIzaSyDz68UPZIjqpQzA-IecyhL8YCw-is72a_s",
  authDomain: "stellasamazingapp.firebaseapp.com",
  databaseURL: "https://stellasamazingapp.firebaseio.com",
  projectId: "stellasamazingapp",
  storageBucket: "stellasamazingapp.appspot.com",
  messagingSenderId: "805938251548"
}

!firebase.apps.length ? firebase.initializeApp(config) : ''

/* if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyDz68UPZIjqpQzA-IecyhL8YCw-is72a_s",
    authDomain: "stellasamazingapp.firebaseapp.com",
    databaseURL: "https://stellasamazingapp.firebaseio.com",
    projectId: "stellasamazingapp",
    storageBucket: "stellasamazingapp.appspot.com",
    messagingSenderId: "805938251548"
  })
} */



//firebase.firestore().settings({ timestampsInSnapshots: true })

//const db = firebase.firestore()
//const storage = firebase.storage() //if use storage
//const base = firebase

//export { base }
//export const GoogleProvider = new firebase.auth.GoogleAuthProvider()
export const auth = firebase.auth()
export const DB = firebase.database()
//export const StoreDB = firebase.firestore()
export default firebase