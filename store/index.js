import Vuex from 'vuex'
import { auth , DB } from '@/plugins/firebase.js'

const Store = () => {
  return new Vuex.Store({
    state: {
        user: null,
        chatroom: null
    },
    mutations: {
		setUser (state, payload) {
            state.user = payload
            //state.user = {...state.user}
        },
        chatroom (state, payload) {
            state.chatroom = payload
            //state.user = {...state.user}
        },
    },
    actions: {
        signout ({commit}) {
            auth.signOut()
            commit('setUser', null)
        },
        signin ({commit}, payload) {
            commit('setUser', payload)
        },
        chatroom ({commit}, payload) {
            commit('chatroom', payload)
        }
    },
    getters: {
        isAuthenticated (state) {
            if(state.user == null) {
                return null
            }
            else {
                return state.user.uid 
            }
        },
        name (state) {
            if(state.user == null) {
                return null
            }
            else {
                return state.user.name 
            }
        }
    }

  })
}

export default Store