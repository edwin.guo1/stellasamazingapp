const pkg = require('./package')
const webpack = require('webpack')
const PurgecssPlugin = require('purgecss-webpack-plugin')
const glob = require('glob-all')
const path = require('path')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: "viewport", content: "width=device-width, initial-scale=1, shrink-to-fit=no" },
      { "http-equiv": "x-ua-compatible", content: "ie=edge" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/icons.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Open+Sans:400,600' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  //loading: { color: '#fff' },
    loading: '~/components/loading.vue',

  /*
  ** Global CSS
  */
  css: [
    { src: 'bulma/bulma.sass', lang: 'sass' },
    { src: '~assets/css/core.css', lang: 'css'}
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      //{ src: '~/plugins/firebase.js', ssr: false }
      { src: '~/plugins/chatscroll', ssr: false}
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    'nuxt-izitoast'
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },
  
  izitoast: {
    position: 'bottomRight',
    transitionIn: 'bounceInLeft',
    transitionOut: 'fadeOutRight',
  },
  generate: {
    fallback: true // if you want to use '404.html' instead of the default '200.html'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    
    //vendor: ['firebase'], 
    
    plugins: [
      new webpack.ProvidePlugin({
        '$': 'jquery'
      })
    ],
    extend(config, ctx) {
        // Run ESLint on save
        if (ctx.isDev && ctx.isClient) {
            config.module.rules.push({
              enforce: 'pre',
              test: /\.(js|vue)$/,
              loader: 'eslint-loader',
              exclude: /(node_modules)/,
              options : {
                fix : true
              }
            })
        }
      
        if (!ctx.isDev) {
          // Remove unused CSS using purgecss. See https://github.com/FullHuman/purgecss
          // for more information about purgecss.
          config.plugins.push(
            new PurgecssPlugin({
              paths: glob.sync([
                path.join(__dirname, './pages/**/*.vue'),
                path.join(__dirname, './layouts/**/*.vue'),
                path.join(__dirname, './components/**/*.vue')
              ]),
              whitelist: ['html', 'body']
            })
          )
        }

    }
  }
}
